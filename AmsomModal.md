## Utilisation

### AmsomModal

Le composant `AmsomModal` est un composant de type modale qui permet d'afficher du contenu dans une fenetre modale. 

#### Props

- `close-option`: Permet d'afficher un bouton de fermeture de la modale. Si cette prop est présente, le composant emet un événement `close` lorsqu'on clique sur le bouton de fermeture.
- `title`: Permet d'afficher un titre sur la modale.
- `position`: Permet de définir la position de la modale. Les valeurs possibles sont `top`, `bottom` et `center`. La valeur par défaut est `center`.
- `size`: Permet de définir la taille de la modale. Les valeurs possibles sont `xl`, `lg`, `md`, `sm`, `xs` et `xxs`. La valeur par défaut est `md`.
- `reducible`: Permet de définir si la modale est réductible. Si cette prop est présente, un bouton de réduction est affiché.

#### Example

```html
<amsom-modal v-if='showModal'>
    content
</amsom-modal>
```

#### Example avec l'option de fermeture

```html
<amsom-modal 
    v-if='showModal'
    close-option
    @close="showModal = false"
>
    content
</amsom-modal>
```

#### Example complet
```tsx
<tempate>
    <amsom-modal v-if='showModal'>
        content
    </amsom-modal>

    <amsom-modal 
        v-if='showSecModal'
        close-option
        @close="showSecModal = false"
    >
        content
    </amsom-modal>
</template>

<script>
import {AmsomModal} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomModal,
    },
    data() {
        return {
            showModal: false,
            showSecModal: false
        }
    }
}
</script>
```
