import AmsomCard from '../AmsomCard.vue';
import AmsomModal from '../AmsomModal.vue';
import AmsomOverlay from '../AmsomOverlay.vue';

export default {
  title: 'AmsomCard',
  component: AmsomCard,
  subcomponents: { AmsomModal, AmsomOverlay },
  tags: ['autodocs'],
  argTypes: {
  },
};

const shortTemplate = `<amsom-card v-bind="args">test</amsom-card>`
const longTemplate = `
<amsom-card v-bind="args">
  start
  <div style="height: 1000px;">
  </div>
  end
</amsom-card>
`

const longTemplateScrollable = `
<amsom-card v-bind="args">
  start
  <div style="height: 1000px;">
  </div>
  end
</amsom-card>
`

const shortTemplateHeader = `
<amsom-card v-bind="args">
  <template #header>
    Header
  </template>
  test
</amsom-card>
`

const shortTemplateFooter = `
<amsom-card v-bind="args">
  test
  <template #footer>
    Footer
  </template>
</amsom-card>
`

const fullCardTemplate = `
<amsom-card v-bind="args">
  <template #header>
    Header
  </template>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  <div style="height: 1000px;">
  </div>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  <template #footer>
    Footer
  </template>
</amsom-card>
`

const cardOverlayTemplate = `
<amsom-overlay loading="loading">
<amsom-card v-bind="args">
<template #header>
Header
</template>
jncjezncjinjcnajcnjncjsncjkqsncjknqsjkcnjknsjkcsn
<template #footer>
Footer
</template>
</amsom-card>
</amsom-overlay>
`

const cardModalTemplate = `

<amsom-card v-bind="args">
<template #header>
Header
</template>
jncjezncjinjcnajcnjncjsncjkqsncjknqsjkcnjknsjkcsn
<template #footer>
Footer
</template>

</amsom-card>
<amsom-modal position="top">
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
  Lorem ipsum dolor sit amet, consectetur adipiscing elit
</amsom-modal>
<amsom-card v-bind="args">
  <template #header>
    Header
  </template>
  jncjezncjinjcnajcnjncjsncjkqsncjknqsjkcnjknsjkcsn
  <template #footer>
    Footer
  </template>
</amsom-card>

`

export const Default = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
  },
};

export const CardWithoutBody = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    noBody: true
  },
};

export const CardLong = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
  },
};


export const CardLongTitled = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    title: "C'est le titre de la carte"
  },
};


export const CardTitled = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    title: "C'est le titre de la carte"
  },
};

export const CardImaged = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    bgImage: "https://fastly.picsum.photos/id/37/2000/2000.jpg?hmac=8pC4znotEGhHTH0iQ39bXXhKO9T8dx6gmLkGjpa9gY8",
  },
};

export const CardImagedScrollable = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: longTemplateScrollable,
  }),
  args: {
    bgImage: "https://fastly.picsum.photos/id/37/2000/2000.jpg?hmac=8pC4znotEGhHTH0iQ39bXXhKO9T8dx6gmLkGjpa9gY8",
    cardStyle: "max-height: 300px;"
  },
};

export const CardImagedCustomOpacity = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    bgImage: "https://fastly.picsum.photos/id/1037/2000/2000.jpg?hmac=AksL_iTuVntFgpcJx8HmU_lQur_N3WkPXjg9ltSV3bs",
    title: "C'est le titre de la carte",
    bgOpacity: 0.8
  },
};


export const CardImageModal = {
  render: (args) => ({
    components: { AmsomCard, AmsomModal },
    setup() {
      return { args };
    },
    template: cardModalTemplate,
  }),
  args: {
    bgImage: "https://fastly.picsum.photos/id/1037/2000/2000.jpg?hmac=AksL_iTuVntFgpcJx8HmU_lQur_N3WkPXjg9ltSV3bs",
    title: "C'est le titre de la carte",
    bgOpacity: 0.8
  },
};

export const CardOverlay = {
  render: (args) => ({
    components: { AmsomCard, AmsomOverlay },
    setup() {
      return { args };
    },
    template: cardOverlayTemplate,
  }),
  args: {
  },
};

export const CardBubblyTitled = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    title: "C'est le titre de la carte 'bubble'",
    bubbleTitle: true,
    bubbleTitleClass:'bg-secondary text-light'
  },
};

export const CardWithHeader = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplateHeader,
  }),
  args: {
    header: true,
    headerClass: "p-4 text-center"
  },
};

export const CardWithFooter = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplateFooter,
  }),
  args: {
    footer: true,
    footerClass: "p-4 text-center"
  },
};

export const CardBodyClass = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    cardClass: 'bg-success text-center fs-5 text-white',
  },
};

export const FullCard = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: fullCardTemplate,
  }),
  args: {
    cardClass: "text-center",
    header: true,
    headerClass: "text-center",
    footer: true,
    footerClass: "text-center",
    bgImage: "https://fastly.picsum.photos/id/1037/2000/2000.jpg?hmac=AksL_iTuVntFgpcJx8HmU_lQur_N3WkPXjg9ltSV3bs",
  },
};

export const FullCardOverflow = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: fullCardTemplate,
  }),
  args: {
    cardClass: "text-center",
    cardStyle: "max-height: 300px;",
    header: true,
    headerClass: "text-center",
    footer: true,
    footerClass: "text-center",
    bgImage: "https://fastly.picsum.photos/id/1037/2000/2000.jpg?hmac=AksL_iTuVntFgpcJx8HmU_lQur_N3WkPXjg9ltSV3bs",
  },
};

export const FullCardAndOptionOverflow = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: fullCardTemplate,
  }),
  args: {
    title: "C'est le titre de la carte 'bubble'",
    bubbleTitle: true,
    bubbleTitleClass:'bg-secondary text-light',
    cardClass: "text-center",
    cardStyle: "max-height: 300px;",
    header: true,
    headerClass: "text-center",
    footer: true,
    footerClass: "text-center",
    bgImage: "https://fastly.picsum.photos/id/1037/2000/2000.jpg?hmac=AksL_iTuVntFgpcJx8HmU_lQur_N3WkPXjg9ltSV3bs",
  },
};

export const FooterAndBubble = {
  render: (args) => ({
    components: { AmsomCard },
    setup() {
      return { args };
    },
    template: shortTemplateFooter,
  }),
  args: {
    title: "C'est le titre de la carte 'bubble'",
    bubbleTitle: true,
    bubbleTitleClass:'bg-secondary text-light',
    cardClass: "text-center",
    footer: true,
    footerClass: "text-center",
    bgImage: "https://fastly.picsum.photos/id/1037/2000/2000.jpg?hmac=AksL_iTuVntFgpcJx8HmU_lQur_N3WkPXjg9ltSV3bs",
  },
};
