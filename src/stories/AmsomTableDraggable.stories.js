import AmsomTableDraggable from '../AmsomTableDraggable.vue';

export default {
  title: 'AmsomTableDraggable',
  component: AmsomTableDraggable,
  tags: ['autodocs'],
  argTypes: {
  },
};

const template = `
<amsom-table-draggable 
  :default-columns="defaultColumns" 
  :td-items-list="tdItemsList" 
  :column-name-date-filter="columnNameDateFilter" 
  :max-item-per-page="maxItemPerPage"
>
  <template #tableBodyTd="tableBodyTdProps">
    <span>{{ tableBodyTdProps.item[tableBodyTdProps.header.id] }}</span> 
  </template>
</amsom-table-draggable>`;

const templateSansCheckBox = `
<amsom-table-draggable 
  :default-columns="defaultColumns" 
  :td-items-list="tdItemsList" 
  :column-name-date-filter="columnNameDateFilter" 
  :max-item-per-page="maxItemPerPage"
>
  <template #tableBodyTd="tableBodyTdProps">
    <span>{{ tableBodyTdProps.item[tableBodyTdProps.header.id] }}</span> 
  </template>
</amsom-table-draggable>`;

const templateAvecBoutonModification = `
<amsom-table-draggable 
  :default-columns="defaultColumns" 
  :td-items-list="tdItemsList" 
  :column-name-date-filter="columnNameDateFilter" 
  :show-modify-columns-btn="showModifyColumnsBtn"
  :max-item-per-page="maxItemPerPage"
>
  <template #tableBodyTd="tableBodyTdProps">
    <span>{{ tableBodyTdProps.item[tableBodyTdProps.header.id] }}</span> 
  </template>
</amsom-table-draggable>`;

const templateEnModeEdition = `
<button type="button" class="btn btn-secondary mb-3" @click="lineEditMode = !lineEditMode">Modifier</button>
<amsom-table-draggable 
  :default-columns="defaultColumns" 
  :td-items-list="tdItemsList" 
  :column-name-date-filter="columnNameDateFilter" 
  :max-item-per-page="maxItemPerPage"
  :line-edit-mode="lineEditMode"
  @line-edit-mode-add-new-line="addNewLine"
  @line-edit-mode-delete-line="deleteLine"
>
  <template #tableBodyTd="tableBodyTdProps">
    <span v-if="!lineEditMode && tableBodyTdProps.header.id === 'name'">{{ tableBodyTdProps.item[tableBodyTdProps.header.id] }}</span> 
    <span v-else-if="lineEditMode && tableBodyTdProps.header.id === 'name'">
      <input type="text" class="form-control" v-model="tableBodyTdProps.item[tableBodyTdProps.header.id]">
    </span>

    <span v-else>{{ tableBodyTdProps.item[tableBodyTdProps.header.id] }}</span> 
  </template>
</amsom-table-draggable>

<span>{{ tdItemsList }}</span>
`;

export const Default = {
  render: (args) => ({
    components: { AmsomTableDraggable },
    setup() {
      return args;
    },
    template: template,
  }),
  args: {
    defaultColumns: [
      { id: 'checkBox', libelle: '', isSortable: false, show: true, isSorted: false },
      { id: 'id', libelle: 'id', isSortable: false, show: true, isSorted: false },
      { id: 'name', libelle: 'Name', isSortable: true, show: true, isSorted: false },
      { id: 'description', libelle: 'Description', isSortable: true, show: true, isSorted: false },
      { id: 'date', libelle: 'Date', isSortable: true, show: true, isSorted: false },
    ],
    tdItemsList: [
      { id: 1, name: 'Item 1', description: 'Description 1', date: '01/01/2024' },
      { id: 2, name: 'Item 2', description: 'Description 2', date: '01/02/2024' },
      { id: 3, name: 'Item 3', description: 'Description 3', date: '01/03/2024' },
      { id: 4, name: 'Item 4', description: 'Description 4', date: '01/04/2024' },
      { id: 5, name: 'Item 5', description: 'Description 5', date: '01/05/2024' }, 
      { id: 6, name: 'Item 6', description: 'Description 6', date: '01/06/2024' },
      { id: 7, name: 'Item 7', description: 'Description 7', date: '01/07/2024' },
      { id: 8, name: 'Item 8', description: 'Description 8', date: '01/08/2024' },
      { id: 9, name: 'Item 9', description: 'Description 9', date: '01/09/2024' },
      { id: 10, name: 'Item 10', description: 'Description 10', date: '01/10/2024' },
    ],
    columnNameDateFilter: ['date'],
    maxItemPerPage: 5,
  },
};

export const SansCheckBox = {
  render: (args) => ({
    components: { AmsomTableDraggable },
    setup() {
      return args;
    },
    template: templateSansCheckBox,
  }),
  args: {
    defaultColumns: [
      { id: 'id', libelle: 'id', isSortable: false, show: true, isSorted: false },
      { id: 'name', libelle: 'Name', isSortable: true, show: true, isSorted: false },
      { id: 'description', libelle: 'Description', isSortable: true, show: true, isSorted: false },
      { id: 'date', libelle: 'Date', isSortable: true, show: true, isSorted: false },
    ],
    tdItemsList: [
      { id: 1, name: 'Item 1', description: 'Description 1', date: '01/01/2024' },
      { id: 2, name: 'Item 2', description: 'Description 2', date: '01/02/2024' },
      { id: 3, name: 'Item 3', description: 'Description 3', date: '01/03/2024' },
      { id: 4, name: 'Item 4', description: 'Description 4', date: '01/04/2024' },
      { id: 5, name: 'Item 5', description: 'Description 5', date: '01/05/2024' }, 
      { id: 6, name: 'Item 6', description: 'Description 6', date: '01/06/2024' },
      { id: 7, name: 'Item 7', description: 'Description 7', date: '01/07/2024' },
      { id: 8, name: 'Item 8', description: 'Description 8', date: '01/08/2024' },
      { id: 9, name: 'Item 9', description: 'Description 9', date: '01/09/2024' },
      { id: 10, name: 'Item 10', description: 'Description 10', date: '01/10/2024' },
    ],
    columnNameDateFilter: ['date'],
    maxItemPerPage: 5,
  }
};

export const AvecBoutonModification = {
  render: (args) => ({
    components: { AmsomTableDraggable },
    setup() {
      return args;
    },
    template: templateAvecBoutonModification,
  }),
  args: {
    defaultColumns: [
      { id: 'checkBox', libelle: '', isSortable: false, show: true, isSorted: false },
      { id: 'id', libelle: 'id', isSortable: false, show: false, isSorted: false },
      { id: 'name', libelle: 'Name', isSortable: true, show: true, isSorted: false },
      { id: 'description', libelle: 'Description', isSortable: true, show: true, isSorted: false },
      { id: 'date', libelle: 'Date', isSortable: true, show: true, isSorted: false },
    ],
    tdItemsList: [
      { id: 1, name: 'Item 1', description: 'Description 1', date: '01/01/2024' },
      { id: 2, name: 'Item 2', description: 'Description 2', date: '01/02/2024' },
      { id: 3, name: 'Item 3', description: 'Description 3', date: '01/03/2024' },
      { id: 4, name: 'Item 4', description: 'Description 4', date: '01/04/2024' },
      { id: 5, name: 'Item 5', description: 'Description 5', date: '01/05/2024' }, 
      { id: 6, name: 'Item 6', description: 'Description 6', date: '01/06/2024' },
      { id: 7, name: 'Item 7', description: 'Description 7', date: '01/07/2024' },
      { id: 8, name: 'Item 8', description: 'Description 8', date: '01/08/2024' },
      { id: 9, name: 'Item 9', description: 'Description 9', date: '01/09/2024' },
      { id: 10, name: 'Item 10', description: 'Description 10', date: '01/10/2024' },
    ],
    columnNameDateFilter: ['date'],
    showModifyColumnsBtn: true,
    maxItemPerPage: 5,
  }
};

export const TableauOverFlow = {
  render: (args) => ({
    components: { AmsomTableDraggable },
    setup() {
      return args;
    },
    template: template,
  }),
  args: {
    defaultColumns: [
      { id: 'checkBox', libelle: '', isSortable: false, show: true, isSorted: false },
      { id: 'id', libelle: 'id', isSortable: false, show: true, isSorted: false },
      { id: 'name', libelle: 'Name', isSortable: true, show: true, isSorted: false },
      { id: 'description', libelle: 'Description', isSortable: true, show: true, isSorted: false },
      { id: 'date', libelle: 'Date', isSortable: true, show: true, isSorted: false },
      { id: 'description2', libelle: 'Description 2', isSortable: true, show: true, isSorted: false },
      { id: 'description3', libelle: 'Description 3', isSortable: true, show: true, isSorted: false },
      { id: 'description4', libelle: 'Description 4', isSortable: true, show: true, isSorted: false },
      { id: 'description5', libelle: 'Description 5', isSortable: true, show: true, isSorted: false },
      { id: 'description6', libelle: 'Description 6', isSortable: true, show: true, isSorted: false },
      { id: 'description7', libelle: 'Description 7', isSortable: true, show: true, isSorted: false },
      { id: 'description8', libelle: 'Description 8', isSortable: true, show: true, isSorted: false },
      { id: 'description9', libelle: 'Description 9', isSortable: true, show: true, isSorted: false },
      { id: 'description10', libelle: 'Description 10', isSortable: true, show: true, isSorted: false },
      { id: 'description11', libelle: 'Description 11', isSortable: true, show: true, isSorted: false },
      { id: 'description12', libelle: 'Description 12', isSortable: true, show: true, isSorted: false },
    ],
    tdItemsList: [
      { id: 1, name: 'Item 1', description: 'Description 1', date: '01/01/2024', description2: 'Description 1', description3: 'Description 1', description4: 'Description 1', description5: 'Description 1', description6: 'Description 1', description7: 'Description 1', description8: 'Description 1', description9: 'Description 1', description10: 'Description 1', description11: 'Description 1', description12: 'Description 1' },
      { id: 2, name: 'Item 2', description: 'Description 2', date: '01/02/2024', description2: 'Description 2', description3: 'Description 2', description4: 'Description 2', description5: 'Description 2', description6: 'Description 2', description7: 'Description 2', description8: 'Description 2', description9: 'Description 2', description10: 'Description 2', description11: 'Description 2', description12: 'Description 2' },
    ],
    columnNameDateFilter: ['date'],
    maxItemPerPage: 5,
  }
};

export const TableauModeEdition = {
  render: (args) => ({
    components: { AmsomTableDraggable },
    setup() {
      const addNewLine = () => {
        args.tdItemsList.unshift({ id: args.tdItemsList.length + 1, name: 'Item ' + (args.tdItemsList.length + 1), date: '01/01/2024'});
      }

      const deleteLine = (id) => {
        args.tdItemsList = args.tdItemsList.filter(item => item.id !== id);
      }

      args.addNewLine = addNewLine;
      args.deleteLine = deleteLine;

      return args;
    },
    template: templateEnModeEdition,
  }),
  args: {
    defaultColumns: [
      { id: 'id', libelle: 'id', isSortable: false, show: true, isSorted: false },
      { id: 'name', libelle: 'Name', isSortable: true, show: true, isSorted: false },
      { id: 'date', libelle: 'Date', isSortable: true, show: true, isSorted: false },
      { id: 'deleteButton', libelle: '', isSortable: false, show: false, isSorted: false },
    ],
    tdItemsList: [
      { id: 8, name: 'Item 8', date: '01/08/2024', lineState: 'add' },
      { id: 7, name: 'Item 7', date: '01/07/2024', lineState: 'delete' },
      { id: 6, name: 'Item 6', date: '01/06/2024', lineState: 'delete' },
      { id: 5, name: 'Item 5', date: '01/05/2024', lineState: 'update' },
      { id: 1, name: 'Item 1', date: '01/01/2024' },
      { id: 2, name: 'Item 2', date: '01/02/2024' },
      { id: 3, name: 'Item 3', date: '01/03/2024' },
      { id: 4, name: 'Item 4', date: '01/04/2024' },
    ],
    columnNameDateFilter: ['date'],
    maxItemPerPage: 999,
    lineEditMode: false,
  }
};