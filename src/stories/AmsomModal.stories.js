import AmsomModal from '../AmsomModal.vue';


export default {
  title: 'AmsomModal',
  component: AmsomModal,
  tags: ['autodocs'],
  argTypes: {
  },
};


const shortTemplate = `<amsom-modal v-bind="args">test</amsom-modal>`
const longTemplate = `
<amsom-modal v-bind="args">
  start
  <div style="height: 1000px;">
  </div>
  end
</amsom-modal>
`
const modalHoverDivTemplate = `
<div class="hover-div bg-danger mx-auto text-light text-center" style="height: 1000px; width: 400px">
  Affichage d'une div quelconque
</div>
<amsom-modal v-bind="args">
  lorem ipsum dolor sit amet consectetur adipisicing elit
</amsom-modal>
`



const secondModalExample = `<amsom-modal size="xxs" closeOption>xxs</amsom-modal>`
const thirdModalExample = `<amsom-modal size="xs" closeOption>xs</amsom-modal>`
const fourthModalExample = `<amsom-modal size="sm" closeOption>sm</amsom-modal>`
const fifthModalExample = `<amsom-modal size="md" closeOption>md</amsom-modal>`
const sixthModalExample = `<amsom-modal size="lg" closeOption>lg</amsom-modal>`
const seventhModalExample = `<amsom-modal size="xl" closeOption>xl</amsom-modal>`

export const Default = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
  },
};

export const ModalScrollableUnclosable = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    closeOption: false
  },
};

export const ModalScrollableClosable = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    closeOption: true
  },
};

export const ModalScrollableTop = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    closeOption: true,
    position: "top"
  },
};

export const ModalScrollableBottom = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    closeOption: true,
    position: "bottom"
  },
};

export const ModalClosable = {
  args: {
    closeOption: true
  }
}

export const ModalNotClosable = {
  args: {
    closeOption: false
  }
}

export const ModalTopPosition = {
  args: {
    position: "top"
  }
}

export const ModalBottomPosition = {
  args: {
    position: "bottom"
  }
}

export const ModalReducible = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: modalHoverDivTemplate,
  }),
  args: {
    reducible: true
  }
}

export const ModalSizeXL = {
  args: {
    size: "xl"
  }
}

export const ModalSizeLG = {
  args: {
    size: "lg"
  }
}

export const ModalSizeMD = {
  args: {
    size: "md"
  }
}

export const ModalSizeSM = {
  args: {
    size: "sm"
  }
}

export const ModalSizeXS = {
  args: {
    size: "xs"
  }
}

export const ModalSizeXXS = {
  args: {
    size: "xxs"
  }
}

export const MultipleModalsOpen = {
  render: (args) => ({
    components: { AmsomModal },
    setup() {
      return { args };
    },
    template: seventhModalExample + sixthModalExample + fifthModalExample + fourthModalExample + thirdModalExample + secondModalExample,
  }),
  args: {
    closeOption: true
  },
};
