import AmsomPagination from '../AmsomPagination.vue';
import {ref} from "vue";

const Template = (args) => ({
  components: { AmsomPagination },
  setup() {
    const page = ref(args.modelValue);
    const updatePage = (newPage) => {
      page.value = newPage;
    };
    return { args, page, updatePage };
  },
  template: '<amsom-pagination v-bind="args" v-model:modelValue="page" @update:modelValue="updatePage" />',
});

export default {
  title: 'AmsomPagination',
  component: AmsomPagination,
  tags: ['autodocs'],
  argTypes: {
  },
};


export const Default = Template.bind({});
Default.args = {
  totalPages: 20,
  modelValue: 1,
};

export const OddPagesDisplayed = Template.bind({});
OddPagesDisplayed.args = {
    totalPages: 20,
    totalDisplayPages: 4,
    modelValue: 1,
};

export const EvenPagesDisplayed = Template.bind({});
EvenPagesDisplayed.args = {
  totalPages: 20,
  totalDisplayPages: 3,
  modelValue: 1,
};
