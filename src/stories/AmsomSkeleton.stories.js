import AmsomSkeleton from '../AmsomSkeleton.vue';


export default {
  title: 'AmsomSkeleton',
  component: AmsomSkeleton,
  tags: ['autodocs'],
  argTypes: {
  },
};


export const Default = {
  args: {
  }
}

export const LoaderCustom = {
  args: {
    config: [12],
    loaderClass: 'text-danger rounded-4',
  }
}

export const RowCustom = {
  args: {
    config: [3,3],
    rowClass: 'justify-content-between',
  }
}
