import AmsomImage from '../AmsomImage.vue';
import { ref, watch } from 'vue';

export default {
  title: 'AmsomImage',
  component: AmsomImage,
  tags: ['autodocs'],
  argTypes: {
  },
};

const template = `<AmsomImage v-bind="args" :images="images" @update:images="images = $event" />`

export const Default = {
  render: (args) => ({
    components: { AmsomImage },
    setup() {
      // Simuler une donnée réactive pour le v-model
      const images = ref(args.images);

      // Mettre à jour args.images quand images change
      watch(images, (newVal) => {
        args.images = newVal;
      });

      return { args, images };
    },
    // Utiliser :images et @update:images pour simuler v-model
    template: template,
  }),
  args: {
  },
};

export const ImagesLimit = {
  render: (args) => ({
    components: { AmsomImage },
    setup() {
      const images = ref(args.images);

      watch(images, (newVal) => {
        args.images = newVal;
      });

      return { args, images };
    },
    template: template,
  }),
  args: {
    imagesLimit : 3,
  },
};

export const Size = {
  render: (args) => ({
    components: { AmsomImage },
    setup() {
      const images = ref(args.images);

      watch(images, (newVal) => {
        args.images = newVal;
      });

      return { args, images };
    },
    template: template,
  }),
  args: {
    imageSizeWidth : '100px',
    imageSizeHeight : '100px',
  },
};

