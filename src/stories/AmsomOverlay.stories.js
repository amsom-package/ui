import AmsomOverlay from '../AmsomOverlay.vue';

export default {
  title: 'AmsomOverlay',
  component: AmsomOverlay,
  tags: ['autodocs'],
  argTypes: {
  },
};

const shortTemplate = `<amsom-overlay v-bind="args">test</amsom-overlay>`
const longTemplate = `
<amsom-overlay v-bind="args">
  start
  <div style="height: 1000px;">
  </div>
  end
</amsom-overlay>
`

export const Default = {
  args: {
    loading: false
  }
}

export const Loading = {
  render: (args) => ({
    components: { AmsomOverlay },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    loading: true
  },
};

export const LoadingWithScroll = {
  render: (args) => ({
    components: { AmsomOverlay },
    setup() {
      return { args };
    },
    template: longTemplate,
  }),
  args: {
    loading: true
  },
};

export const LoadingOpacityMax = {
  render: (args) => ({
    components: { AmsomOverlay },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    loading: true,
    opacity: 100
  },
};

export const LoadingOpacityMin = {
  render: (args) => ({
    components: { AmsomOverlay },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    loading: true,
    opacity: 0
  },
};

export const NotLoading = {
  render: (args) => ({
    components: { AmsomOverlay },
    setup() {
      return { args };
    },
    template: shortTemplate,
  }),
  args: {
    loading: false
  },
};