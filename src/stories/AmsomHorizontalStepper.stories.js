import AmsomHorizontalStepper from '../AmsomHorizontalStepper.vue';


export default {
  title: 'AmsomHorizontalStepper',
  component: AmsomHorizontalStepper,
  tags: ['autodocs'],
  argTypes: {
  },
};

export const Default = {
  args: {},
};

export const LargeCustomStepper = {
  args: {
    size: 'lg',
    currentStep: 2,
    checkPassedStep: true,
    futureStepBgColor: 'danger',
    currentStepBgColor: 'success',
    passedStepBgColor: 'warning',
    futureStepTextCss: 'text-info',
    currentStepTextCss: 'text-success fw-bold',
    passedStepTextCss: 'text-danger',
    futureStepCss: 'text-primary',
    currentStepCss: 'text-warning fw-bold',
    passedStepCss: 'text-danger',
  },
};
