import AmsomDocumentList from '../AmsomDocumentList.vue';

const documentList = [
  {
    id: 1,
    titre: 'Avis d\'imposition',
    categorie: 'Impôts',
    dateFichier: "1721585280",
    url: "https://pdfobject.com/pdf/sample.pdf"
  },
  {
    id: 2,
    titre: 'Bulletin de salaire',
    categorie: 'Salaire',
    dateFichier: "1720583280",
    url: "https://pdfobject.com/pdf/sample.pdf"
  },
  {
    id: 3,
    titre: 'Carte d\'identité',
    categorie: 'Identité',
    dateFichier: "1622485280",
    url: "https://pdfobject.com/pdf/sample.pdf"
  },
  {
    id: 4,
    titre: "Avis d'imposition",
    categorie: 'Impôts',
    dateFichier: "1722585580",
    url: "https://pdfobject.com/pdf/sample.pdf"
  }
]

const getBlob = async (document) => {
  const response = await fetch(document.url);
  const blob = await response.blob();
  return blob;
}

export default {
  title: 'AmsomDocumentList',
  component: AmsomDocumentList,
  tags: ['autodocs'],
  argTypes: {
  },
};


export const Default = {
  args: {
    fileList: []
  }
}

export const Loading = {
  args: {
    loading: true,
    documentList: []
  }
}

export const DocumentList = {
  args: {
    loading: false,
    title: 'Documents trop importants',
    documentList: documentList,
    getDocumentBlob: getBlob
  }
}

export const DocumentListAnonym = {
  args: {
    loading: false,
    documentList: documentList,
    anonym: true,
    getDocumentBlob: getBlob
  }
}

export const ViewMode = {
  args: {
    loading: false,
    clickAction: 'view',
    documentList: documentList,
    getDocumentBlob: getBlob
  }
}
