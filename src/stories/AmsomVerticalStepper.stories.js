import AmsomVerticalStepper from '../AmsomVerticalStepper.vue';


export default {
  title: 'AmsomVerticalStepper',
  component: AmsomVerticalStepper,
  tags: ['autodocs'],
  argTypes: {
  },
};

export const Default = {
  args: {},
};

export const SmallStepper = {
  args: {
    size: 'sm',
    currentStep: 2,
  },
};

export const StepperWithoutText = {
  args: {
    size: 'sm',
    currentStep: 2,
    hideText: true,
  },
};

export const StepperCustom = {
  args: {
    size: 'sm',
    currentStep: 2,
    checkPassedStep: false,
    futureStepBgColor: 'danger',
    currentStepBgColor: 'info',
    passedStepBgColor: 'warning',
    futureStepTextCss: 'text-info',
    currentStepTextCss: 'text-success fw-bold',
    passedStepTextCss: 'text-danger',
    futureStepCss: 'text-primary',
    currentStepCss: 'text-secondary',
    passedStepCss: 'text-danger',
  },
};
