import moment from 'moment-timezone'

export function normalize(chaine, lower = true) {
  if (!chaine) return null
  let res = chaine
    .toString()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')

  if (lower) return res.toLowerCase()

  return res
}

export function toCurrencyEUR(amount) {
  return amount
    ? amount?.toLocaleString('fr-FR', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2
      })
    : '0 €'
}

export function unixToDateString(unix, format = 'DD/MM/YYYY') {
  if (!moment.unix(unix).isValid()) return unix

  return moment.unix(unix).format(format)
}

export function unixToDateTimeString(unix, format = 'DD/MM/YYYY à HH:mm') {
  return unixToDateString(unix, format)
}

export function howMuchTimeAgo(date) {
  if (!moment.unix(date).isValid()) return date
  return moment.unix(date).fromNow(true)
}

export function dateToUnix(date) {
  return moment(date).unix()
}
