export function updateArrayColumnsPreferences(preferencesId, defaultColumns) {
  let preferences = getPreferences(preferencesId)

  if (!preferences) {
    preferences = {
      columns: defaultColumns
    }
  } else if (!preferences.columns) {
    preferences.columns = defaultColumns
  } else {
    //récupères les colonnes qui ne sont pas dans les preferences mais sont dans les colonnes par defaut
    let newColumns = defaultColumns.filter(function (col) {
      return !preferences.columns.some(function (pref) {
        return pref.id === col.id
      })
    })

    // ajoute les nouvelles colonnes auw preferences
    preferences.columns = preferences.columns.concat(newColumns)

    //récupères les colonnes sont dans les preferences mais sont pas dans les colonnes par defaut
    let oldColumns = preferences.columns.filter(function (pref) {
      return !defaultColumns.some(function (col) {
        return pref.id === col.id
      })
    })

    //récupères les colonnes sont dans les preferences mais sont pas dans les colonnes "old"
    preferences.columns = preferences.columns.filter(function (pref) {
      return !oldColumns.some(function (col) {
        return pref.id === col.id
      })
    })

    //Vérifie que les libelles sont bon
    preferences.columns.filter((pref) => {
      for (let col of defaultColumns) {
        if (col.id === pref.id && col.libelle !== pref.libelle) {
          pref.libelle = col.libelle
        }
      }
    })
  }

  setPreferences(preferencesId, preferences)
}

export function updateShowStatusPreferences(preferencesId, defaultValue) {
  let preferences = getPreferences(preferencesId)
  console.log(preferences)
  if (!preferences) {
    preferences = {
      showStatus: defaultValue
    }
  } else if (!preferences.showStatus) {
    preferences.showStatus = defaultValue
  } else {
    let temp = preferences.showStatus
    for (let status of Object.keys(preferences.showStatus)) {
      if (defaultValue[status] === undefined) delete temp[status]
    }

    for (let status of Object.keys(defaultValue)) {
      if (temp[status] === undefined) temp[status] = true //par defaut on affiche le status
    }
  }

  setPreferences(preferencesId, preferences)
}

export function updateDataPreferences(preferencesId, defaultValue, index = null) {
  let preferences = getPreferences(preferencesId)

  if (!preferences) {
    if (index) {
      preferences = {}
      preferences[index] = defaultValue
    } else {
      preferences = defaultValue
    }
  } else if (index && !preferences[index]) {
    preferences[index] = defaultValue
  } else {
    //todo si on doit verifier l'ordre, la dispo des data, si elles sont dans les possibilités etc...
  }

  setPreferences(preferencesId, preferences)
}

export function getPreferences(preferencesId) {
  return JSON.parse(window.localStorage.getItem(preferencesId))
}

export function setPreferences(preferencesId, value, index = null) {
  let preferences = getPreferences(preferencesId)

  if (index) {
    preferences[index] = value
  } else {
    preferences = value
  }

  return window.localStorage.setItem(preferencesId, JSON.stringify(preferences))
}
