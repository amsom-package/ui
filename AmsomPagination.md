## Utilisation

### AmsomPagination

Le composant `AmsomPagination` est un composant qui affiche un stepper horizontalement. 

#### Props

- `modelValue` : La valeur de la page actuelle du stepper
- `totalPages` : Le nombre total de pages du stepper
- `totalDisplayPages` : Le nombre total de pages affichées dans le stepper

#### Example complet
```tsx
<tempate>
    <amsom-pagination :totalPages='totalPages?.length ?? 0' v-model="currentStep" :totalDisplayPages="3"/>
</template>

<script>
import {AmsomPagination} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomPagination,
    },
    data() {
        return {
            currentStep: 1,
            totalPages: [
                .....
            ],
        }
    }
}
</script>
```
