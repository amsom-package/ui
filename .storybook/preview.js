/** @type { import('@storybook/vue3').Preview } */
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faUsers, faEuroSign, faPen } from '@fortawesome/free-solid-svg-icons';

library.add(faUsers, faEuroSign, faPen);

const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;
