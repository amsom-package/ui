## Utilisation

### AmsonUploadFile

### Installation

Importer les css dans tel que :
```javascript
import '@amsom-habitat/ui/dist/style.css'
```

#### Props

- `name` : Nom de l'input
- `pjName` : Nom de la pj à afficher
- `fileName` : Nom du fichier à stocker
- `subText` : Indication complementaire
- `title` : Titre une fois le fichier chargé
- `required` : Indique si le fichier est requis
- `label` : Label si il est différent de pjName pour l'input
- `modelValue` : Objet contenant les valeurs du fichier
- `gedFile` : Objet contenant le fichier ged le cas echeant
- `readStatus` : Indique l'etat de lecture du fichier
- `readOnly` : Indique si le champ est en lecture seule
- `rejectStatus` : Indique si le fichier a été rejeté
- `clickFileHandler` : Surcharge de la fonction à appeler lors du click sur le fichier

#### Emits

- `delete-file` : Quand le fichier est supprimé
- `file-loading` : Quand le chargement change de status


#### Example complet
```tsx
<tempate>
    <span v-if="fileLoadingCount > 0">Chargement en cours...</span>
    <amsom-upload-file
      v-model="individu.pjCMI"
      required
      name="cmi"
      pj-name="Carte Mobilité Inclusion"
      label="Carte Mobilité Inclusion recto/verso *"
      fileName="TOTO Dupont - Carte mobilité inclusion"
      title="Pièce(s) justificative(s)"
      @delete-file="delete individu.pjCMI"
      @file-loading="updateFileLoading"
    />
</template>

<script>
import {AmsomUploadFile} from '@amsom-habitat/ui'

  export default {
    name: 'TestPage',
    components: {
      AmsomUploadFile,
    },
    data() {
        return {
            individu: {
              id: 1,
              nom: 'Dupont',
              prenom: 'Toto'
            },
            fileLoadingCount: 0
        }
    },
    methods: {
      updateFileLoading(isLoading) {
          if (isLoading) this.fileLoadingCount++;
          else this.fileLoadingCount--;
      }
    }
}
</script>
```
