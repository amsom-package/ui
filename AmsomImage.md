## Utilisation

### AmsomImage

Le composant `AmsomImage` est un composant qui gère les images. Il ajoute, compresse et supprime les images de façon autonome. 

#### Props

- `images`: Valeur du v-model:images="".
- `imageSizeWidth`: Permet de définir la largeur des éléments (button + images). Par defaut `5Opx`.
- `imageSizeHeight`: Permet de définir la hauteur des éléments (button + images). Par defaut `5Opx`.
- `imagesLimit`: Permet de limiter le nombre d'image que le composant peut prendre. Par defaut `-1` (sans limite).
- `capture`: Argument principalement utilisé par les smartphones afin d'utiliser la caméra avant (`capture="user"`) ou la caméra arrière (`capture="environment"`). Par defaut `file`.

#### Example

```html
<amsom-image v-model:images='listImages' />
```

#### Example avec les options de taille

```html
<amsom-image v-model:images='listImages' imageSizeWidth='100px' imageSizeHeight='100px' />
```

#### Example avec une limite du nombre d'image

```html
<amsom-image v-model:images='listImages' :imagesLimit='1' />
```

#### Example avec la camera avant sur un smartphone

```html
<amsom-image v-model:images='listImages' capture='user' />
```

#### Example complet
```tsx
<tempate>
  <amsom-image v-model:images='listImages' />
</template>

<script>
import { AmsomImage } from '@amsom-habitat/ui'

export default {
  name: 'TestPage',
  components: {
    AmsomImage,
  },
  data() {
    return {
      listImages: []
    }
  }
}
</script>
```