## Utilisation

### AmsomDocumentList

Le composant `AmsomDocumentList` est un composant qui affiche une liste de documents et permet d'interagir avec. 

#### Props

- `title` : Le titre de la liste de documents
- `loading` : L'état de chargement de la liste de documents
- `documentList` : La liste des documents à afficher (doit contenir les propriétés `categorie` et `dateFichier`)
- `anonym` : L'état d'anonymat de la liste de documents
- `getDocumentBlob` : La fonction qui permet de récupérer le blob du document
- `clickAction` : L'action à effectuer lors du clic sur un document (download ou view)

#### Example complet
```tsx
<tempate>
    <amsom-document-list 
        :document-list="documentList"
        :loading="false"
        title="Mes documents"
        :anonym="false"
        :getDocumentBlob="getDocumentBlob"
        clickAction="download"
    />
</template>

<script>
import {AmsomDocumentList} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomDocumentList,
    },
    data(){
        return {
            documentList: [
                {
                    id: 1,
                    titre: 'Avis d\'imposition',
                    categorie: 'Impôts',
                    dateFichier: "1721585280",
                    url: "https://pdfobject.com/pdf/sample.pdf"
                },
                {
                    id: 2,
                    titre: 'Bulletin de salaire',
                    categorie: 'Salaire',
                    dateFichier: "1720583280",
                    url: "https://pdfobject.com/pdf/sample.pdf"
                }
            ]
        }
    },
    methods: {
        getDocumentBlob(document){
            const response = await fetch(document.url);
            const blob = await response.blob();
            return blob;
        }
    }
}
</script>
```
