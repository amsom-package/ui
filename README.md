# AMSOM UI

Ce package regroupe plusieurs composant déstiné à l'UI des outils d'AMSOM Habitat est est disponible [ici](https://www.chromatic.com/library?appId=65e972814b5f7a6462f17d16)

Il regroupe les packages :

- AmsomModal voir la documentation [ici](AmsomModal.md)
- AmsomOverlay voir la documentation [ici](AmsomOverlay.md)
- AmsomCard voir la documentation [ici](AmsomCard.md)
- AmsomVerticalStepper voir la documentation [ici](AmsomVerticalStepper.md)
- AmsomHorizontalStepper voir la documentation [ici](AmsomHorizontalStepper.md)
- AmsomUploadFile voir la documentation [ici](AmsomUploadFile.md)
- AmsomDocumentList voir la documentation [ici](AmsomDocumentList.md)
- AmsomImage voir la documentation [ici](AmsomImage.md)
- AmsomPagination voir la documentation [ici](AmsomPagination.md)
- AmsomSkeleton voir la documentation [ici](AmsomSkeleton.md)
- AmsomTableDraggable voir la documentation [ici](AmsomTableDraggable.md)

## Installation

```bash
npm i @amsom-habitat/ui
```
Importer les css dans le main.js tel que :
```javascript
import '@amsom-habitat/ui/dist/style.css'
```


## Développment

Après avoir fait vos dev, veillez à bien tenir à jour le [changelog.md](changelog.md) ainsi que la version du package.json puis faites :
```bash
git add .
git commit -m '<commentaire'
git push origin <branch>
```

## Tests

Les tests sont réalisé de manière automatique sur les branches main et dev mais peuvent être fait localement, notemment pour voir l'evolution du développement via la commande :
```bash
npm run storybook
```

Le valideur devra, si des changements sont observés, aller sur la pipeline pour valider les différences à l'aide de chromatic, sans cela aucun merge-request ne sera possible. Si un merge est effectué, une double verification sera necessaire.

## Déploiement

Après avoir merge les dev sur la branche main, exécutez :
```bash
make publish
```
Cette commande vérifie la version, le changelog et publie le tout
