## Utilisation

### AmsomSkeleton

Le composant `AmsomSkeleton` est un composant qui affiche un des squelettes de loader horizontaux. 

#### Props

- `config` : La configuration du skeleton avec un tableau de breakpoints de col bootstrap
- `rowClass` : Classe(s) à ajouter à la div row
- `loaderClass` : Classe(s) à définir à la div du loader

#### Example complet
```tsx
<tempate>
    <amsom-skeleton 
        :config="[3,3]"
        rowClass="justify-content-between"
        loaderClass="text-danger rounded-4"
    />
</template>

<script>
import {AmsomSkeleton} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomSkeleton,
    }
}
</script>
```
