## Utilisation

### AmsomTableDraggable

Le composant `AmsomTableDraggable` est un composant tableau qui permet de rendre intéractif les colonnes du tableau. 
On a la possibilité de : déplacer les colonnes, afficher ou non une colonne, filtrer les colonnes, sélectionner des lignes ainsi q'un système de pagination intégré.

#### Props

- `defaultColumns`: Les colonnes a affiché dans les entêtes. 
Chaque objet de la liste doit être dans ce format : 
```json
{ id: 'nom', libelle: 'Nom', isSortable: false, show: true, isSorted: false }
```

**Remarques** : Pour les checkbox, il faut ajouter ceci en premier élément de votre liste de colonne : 
```json
{ id: 'checkBox', libelle: '', isSortable: false, show: true, isSorted: false }
```

- `tdItemsList` : Liste des éléments qui seront affichés dans chacune des lignes du tableau. **Les `id` des colonnes doivent avoir le même noms que les attributs des objets à afficher dans le tableau**

**Remarques coloration des lignes** : Les lignes peuvent avoir des couleurs d'affichage différentes (add: success, delete: danger, update: warning)
```json
{ id: 1, name: 'Item 1', date: '01/08/2024', lineState: 'add' },
{ id: 2, name: 'Item 2', date: '01/08/2024', lineState: 'delete' },
{ id: 3, name: 'Item 3', date: '01/08/2024', lineState: 'update' },
```

- `localStorageItemName` : Permet d'enregistrer les préférences d'affichage dans le localStorage. Par défaut, il est à `null`, c'est-à-dire qu'il n'est pas activé. Il faut un nom pour le rendre actif.

- `showModifyColumnsBtn` : Affiche ou non le bouton de modification du tableau pour afficher ou non une colonne. Par défaut, il est à `false`. Pour rendre ce bouton externe, il faut mettre une référence sur le composant et appelé ces différents fonctions : 
  - `editTablePreferences()` : Passe le tableau en mode `edition`
  - `saveTablePreferences()` : Sauvegarde les préférences. Aussi dans le `localStorage` s'il est actif.
  - `resetTablePreferences()` : Réunitialise les colonnes avec les valeurs par défaut de `defaultColumn`

  Exemple : 
  ```html
    @edit-table-preferences="() => $refs.tableDraggableComponent.editTablePreferences()"
    @save-table-preferences="() => $refs.tableDraggableComponent.saveTablePreferences()"
    @reset-table-preferences="() => $refs.tableDraggableComponent.resetTablePreferences()"
  ```

- `maxItemPerPage` : Nombre de ligne que le tableau peux afficher au maximum. Par défaut, c'est `10 lignes`. C'est une valeur calculée. Si le nombre de ligne est supérieur au nombre max de ligne, une pagination intégrée sera active automatiquement.

- `search` : Valeur de recherche qui permet de filtrer les lignes en fonction du contenu chaque ligne et colonne.

- `pickedDate` : Permet de filtrer les lignes du tableaux en fonction des dates. C'est un tableau de deux valeurs `moment`.

- `columnNameDateFilter` : Liste des colonnes qui ont des dates en `UNIX`. Le nom de ces colonnes doivent impérativement commencer par `date`. 
Les valeurs de `pickedDate` vont être comparé avec ces colonnes.

- `clickable` : Permet de rendre une ligne clickable. Par défault, il est à `false`.

- `ghostClass` : Change la couleur du background lors du drag. Par défault, il a `bg-success`.

- `@changeSubject` : Permet de récupérer la ligne sélectionnée. Fonctionne uniquement si `clickable` est à `true`.

- `@updateSelectedItems` : Récupère la liste des attributs `id` des lignes sélectionnées.

- `lineEditMode` : Permet de faire passer le tableau en mode édition (`true` ou `false`)

- `@lineEditModeAddNewLine` : Appelle une fonction parent pour ajouter une ligne si `lineEditMode` est à `true`

- `@lineEditModeDeleteLine` : Appelle une fonction parent pour supprimer une ligne si `lineEditMode` est à `true`

**Remarques** : Pour le bouton de suppression de ligne, il faut ajouter ceci en dernier élément de votre liste de colonne : 
```json
{ id: 'deleteButton', libelle: '', isSortable: false, show: false, isSorted: false },
```

#### Example complet
```tsx
<tempate>
<amsom-table-draggable 
  :default-columns="defaultColumns" 
  :td-items-list="tdItemsList" 
  :column-name-date-filter="columnNameDateFilter" 
  :show-modify-columns-btn="showModifyColumnsBtn"
  :max-item-per-page="maxItemPerPage"
  :search="search"
  :picked-date="pickedDate"
  clickable
  @change-subject="selectNewSubject"
  @update-selected-items="updateSelectedItems"
>
  <template #tableBodyTd="tableBodyTdProps">
    <span v-if="tableBodyTdProps.header.id.startsWith('date')">
      {{ unixToDate(tableBodyTdProps.item[tableBodyTdProps.header.id]) }}
    </span>
    <span v-else>{{ tableBodyTdProps.item[tableBodyTdProps.header.id] }}</span> 
  </template>
</amsom-table-draggable>
</template>

<script>
import { AmsomTableDraggable } from '@amsom-habitat/ui'

export default {
  name: 'TestPage',
  components: {
    AmsomTableDraggable,
  },
  data() {
    return {
      defaultColumns: [
        { id: 'checkBox', libelle: '', isSortable: false, show: true, isSorted: false },
        { id: 'id', libelle: 'id', isSortable: false, show: true, isSorted: false },
        { id: 'name', libelle: 'Name', isSortable: true, show: true, isSorted: false },
        { id: 'description', libelle: 'Description', isSortable: true, show: true, isSorted: false },
        { id: 'date', libelle: 'Date', isSortable: true, show: true, isSorted: false },
      ],
      tdItemsList: [
        { id: 1, name: 'Item 1', description: 'Description 1', date: 1610236800000 },
        { id: 2, name: 'Item 2', description: 'Description 2', date: 1610236800000 },
        { id: 3, name: 'Item 3', description: 'Description 3', date: 1610236800000 },
        { id: 4, name: 'Item 4', description: 'Description 4', date: 1610236800000 }
      ],
      columnNameDateFilter: ['date'],
      showModifyColumnsBtn: true,
      maxItemPerPage: 5,
      search: '',
      pickedDate: null,
      localStorageItemName: 'tableDraggableArrayPreferences',
      selectedItems: [],
    }
  },
  methods: {
    unixToDate() {
      // Convertit le unix en date lisible
    },

    selectNewSubject(value) {
      // Faire quelque chose avec la valeur
    },

    updateSelectedItems(value) {
      this.selectedItems = value
    },
  }
}
</script>
```