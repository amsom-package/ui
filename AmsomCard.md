## Utilisation

### AmsomCard

Le composant `AmsomCard` est un composant qui affiche du contenu dans une carte. 

#### Props

- `title`: Permet de définir un titre.
- `bgImage`: Permet de définir une image de fond.
- `opacity`: Permet de définir l'opacité de l'image de fond.
- `noBody`: Permet d'enlever le padding du body de la carte.
- `bubbleTitle`: Booléen, associé à `title` il permet de déclencher l'affichage de `title` en mode 'bubble' dont il est possible de modifier l'affichage via la props `bubbleTitleClass`.
- `bubbleTitleClass`: Permet de définir une classe CSS pour le titre en mode 'bubble'.
- `cardClass`: Permet de définir une classe CSS pour la carte.
- `footer`: Permet d'ajouter un footer à la carte.
- `footerClass`: Permet de définir une classe CSS pour le footer.
- `header`: Permet d'ajouter un header à la carte.
- `headerClass`: Permet de définir une classe CSS pour le header.


#### Example avec titre
```tsx
<tempate>
    <amsom-card title='Veuillez valider ces informations'>
        <span>content</span>
    </amsom-card>
</template>
```

#### Example avec fond
```tsx
<tempate>
    <amsom-card bgImage='https://fastly.picsum.photos/id/37/2000/2000.jpg?hmac=8pC4znotEGhHTH0iQ39bXXhKO9T8dx6gmLkGjpa9gY8'>
        <span>content</span>
    </amsom-card>
</template>
```

#### Example avec fond et opacité custom
```tsx
<tempate>
    <amsom-card bgImage='https://fastly.picsum.photos/id/37/2000/2000.jpg?hmac=8pC4znotEGhHTH0iQ39bXXhKO9T8dx6gmLkGjpa9gY8' opacity="0.8">
        <span>content</span>
    </amsom-card>
</template>
```

#### Example sans titre
```tsx
<tempate>
    <amsom-card>
        <span>content</span>
    </amsom-card>
</template>
```


#### Example complet
```tsx
<tempate>
    <amsom-card>
        <span>content</span>
    </amsom-card>
</template>

<script>
import {AmsomCard} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomCard,
    }
}
</script>
```
