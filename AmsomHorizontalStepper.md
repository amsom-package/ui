## Utilisation

### AmsomHorizontalStepper

Le composant `AmsomHorizontalStepper` est un composant qui affiche un stepper horizontalement. 

#### Props

- `steps` : Tableau des étapes du stepper. Chaque étape est un objet avec les propriétés suivantes :
  - `numero` : Numéro de l'étape
  - `texte` : Texte de l'étape
- `currentStep` : Numéro de l'étape courante
- `size` : Taille du stepper. Peut prendre les valeurs `sm`, `md` ou `lg`
- `checkPassedStep` : Indique si les numéros des étapes passées doivent être un check ou non
- `passedStepBgColor` : Couleur de fond des étapes passées
- `currentStepBgColor` : Couleur de fond de l'étape courante
- `futureStepBgColor` : Couleur de fond des étapes futures
- `passedStepCss` : Classe CSS des bulles étapes passées
- `currentStepCss` : Classe CSS de la bulle l'étape courante
- `futureStepCss` : Classe CSS des bulles des étapes futures
- `passedStepTextCss` : Classe CSS du texte des étapes passées
- `currentStepTextCss` : Classe CSS du texte de l'étape courante
- `futureStepTextCss` : Classe CSS du texte des étapes futures
- `hideText` : Indique si le texte des étapes doit être caché

#### Example complet en definissant les etapes et l'etape courante
```tsx
<tempate>
    <amsom-horizontal-stepper :steps='steps' :currentStep="currentStep" @clickStep="handleClickStep"/>
</template>

<script>
import {AmsomHorizontalStepper} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomHorizontalStepper,
    },
    data() {
        return {
            currentStep: 1,
            steps: [
                {numero: 1, texte: 'Step 1'},
                {numero: 2, texte: 'Step 2'},
                {numero: 3, texte: 'Step 3'},
                {numero: 4, texte: 'Step 4'},
                {numero: 5, texte: 'Step 5'},
            ],
        }
    },
    methods: {
        handleClickStep(step) {
            if(step < this.currentStep) {
                this.currentStep = step
            }
        }
    }
}
</script>
```
