## Utilisation

### AmsomOverlay

Le composant `AmsomOverlay` est un composant qui affiche un loader, au besoin par dessus le contenu. 

#### Props

- `loading`: Permet d'afficher ou non le loader.
- `opacity`: Permet de définir l'opacité de l'arrière plan de l'Overlay (min 0 / max 100)

#### Example complet en definissant l'opacité
```tsx
<tempate>
    <amsom-overlay :loading='isLoading' :opacity="80">
        <h1>titre</h1>
        <span>content</span>
    </amsom-overlay>

    <button @click="isLoading = true">
        Start load
    </button>
    <button @click="isLoading = false">
        Stop load
    </button>
</template>

<script>
import {AmsomOverlay} from '@amsom-habitat/ui'

export default {
    name: 'TestPage',
    components: {
        AmsomOverlay,
    },
    data() {
        return {
            isLoading: false
        }
    }
}
</script>
```

#### Example sans définir l'opacité (prend la valeur par défaut)
```tsx
<tempate>
    <amsom-overlay :loading='isLoading'>
        <h1>titre</h1>
        <span>content</span>
    </amsom-overlay>
</template>
```