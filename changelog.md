# Changelog
<!-- introduction au changelog -->
Le projet UI est un regroupement de packages UI destinés aux devs de AMSOM Habitat 

## V2.14.0 - 06/08/2024

### Fixed

- Correction de l'update de la size de la modal qui fait planter les formulaire (focus)

## V2.13.0 - 02/08/2024

### Added

- Nouveau composant 'AmsomUploadFile' est un composant d'envoie de fichier

## V2.12.0 - 02/08/2024

### Added

- Nouveau composant 'AmsomSkeleton' est un composant qui affiche des squelettes de loader horizontaux
- Nouveau composant 'AmsomDocumentList' est un composant qui affiche une liste de documents et permet d'interagir avec


## V2.11.0 - 31/07/2024

### Added

- Nouveau composant 'AmsomPagination' est un composant qui gère la pagination (selection de page)


## V2.10.0 - 28/07/2024

### Added

- Nouveau composant 'AmsomImage' est un composant d'image d'upload/affichage des images
- Nouveau composant 'AmsomTableDraggale' est un composant de tableau draggable


## V2.9.0 - 14/07/2024

### Changed

- 'AmsomModal' offre désormais des tailles plus variées sur la props 'size'
- 'AmsomModal' accepte désormais la possibilité de réduire la modale et de la restaurer

## V2.8.0 - 12/07/2024

### Changed

- 'AmsomCard' ne s'appuie désormais plus sur un z-index
- Corrections diverses suite à la publication de la version 2.7.0

## V2.8.0 - 11/07/2024

### Fixed
- Correction bug z-index


## V2.7.0 - 03/07/2024

### Added

- 'AmsomModal' offre la possibilité de se fermer avec la touche 'ESC'
- 'AmsomModal' accepte désormais les props 'size' et 'position'
- 'AmsomModal' accepte désormais la superposition de plusieurs modals
- 'AmsomModal' est plus adapté pour les petits appareils (responsive)
- 'AmsomModal' n'accepte plus le scroll en arrière plan (site derrière la modal)
- 'AmsomCard' accepte désormais une props 'header' pour ajouter une en-tête
- 'AmsomCard' accepte désormais une props 'headerClass'
- 'AmsomCard' accepte désormais un slot 'header'
- 'AmsomCard' accepte désormais une props 'cardClass'
- 'AmsomCard' accepte désormais la props 'footer' pour ajouter un bas de page
- 'AmsomCard' accepte désormais la props 'footerClass'

## V2.6.0 - 24/06/2024

### Added

- 'AmsomCard' accepte désormais une props 'bubbleTitle'
- 'AmsomModal' accepte désormais une props 'Title'

## V2.5.0 - 15/05/2024

### Added

- 'AmsomCard' peut désormais prendre "noBody" en paramètre pour enlever les paddings du body

## V2.4.1 - 13/05/2024

### Changed

- 'AmsomOverlay' peut désormais prendre des classes CSS personnalisées

## V2.4.0 - 13/05/2024

### Added

- 'AmsomCard' peut désormais prendre "bgImage" en paramètre pour afficher une image de fond
- 'AmsomCard' peut désormais prendre "bgOpacity" en paramètre pour régler l'opacité de l'image de fond

## V2.3.0 - 03/05/2024

### Changed

- 'AmsomHorizontalStepper' qui prend beaucoup plus de paramétrage et est documenté
- 'AmsomVerticalStepper' qui prend beaucoup plus de paramétrage et est documenté

### Removed

- 'AmsomHorizontalWorkflow' qui ne sert a rien


## V2.2.0 - 03/05/2024

### Added

- Nouveau composant 'AmsomHorizontalStepper'
- Nouveau composant 'AmsomHorizontalWorkflow'


## V2.1.0 - 03/05/2024

### Added

- Nouveau composant 'AmsomVerticalStepper'


### Changed

- Import des icons par defaut dans le composant directement afin de marcher meme si l'utilisateur n'importe pas


## V2.0.0 - 05/03/2023

### Added

- Gestion du versionning
- Linter
- Script de deploiment

### Fixed

- Erreurs du linter (multiwordName)


## V0.0.0 - 05/03/2023

### Added

- blabla
- blabla

### Fixed

- blabla
- blabla

### Changed

- blabla
- blabla

### Removed

- blabla
- blabla
